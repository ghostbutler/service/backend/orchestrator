Orchestractor
=============

Introduction
------------

The orchestrator is responsible for area IFTTT rules save and application

API documentation
-----------------

*Rules API*:

- GET /api/v1/orchestrator/rules?uuid=
  
  Get a rule details by its uuid
  
  Parameter(s):
    - uuid: the rule uuid to get description of (GET)

  Response:
    - 200 OK: response contains rule description
    - 400 Bad Request: uuid is incorrect

- POST /api/v1/orchestrator/rules

  Create a new rule with its full declaration

  Parameter(s):
    - rule content without uuid (json format)

  Response:
    - 200 OK: rule was successfully created and added to redis
    - 400 Bad Request: rule is incorrect, response is json and gives the error
    message into "error" field 

- PUT /api/v1/orchestrator/rules

  Edit an existing rule, has to be provided full rule
  definition with rule uuid to edit (json format)
  
  Parameter(s):
    - rule content with uuid (json format)
    
  Response:
    - 200 OK: rule was successfully created and added to redis
    - 400 Bad Request: rule is incorrect, response is json and gives the error
    message into "error" field 

- GET /api/v1/orchestrator/rules/list

  List existing rules, returns an array of rules name/description/uuid
  
  Response:
    - 200 OK: returns the rules list
    - 500 Internal Server Error: an unknown error occured

- DELETE /api/v1/orchestrator/rules

  Delete a rule by its uuid
  
  Parameter(s):
    - uuid: the rule uuid to remove (GET)
  
  Response:
    - 200 OK: the rule was successfully removed
    - 400 Bad Request: the rule could not be found

- GET /api/v1/orchestrator/actions?uuid=

- POST /api/v1/orchestrator/actions

Example, an action which will light on an outlet named "PriseVentilateur", and change light named "LumierePorte" to red color:

```bash
curl -k -v https://127.0.0.1:16565/api/v1/orchestrator/actions -X POST -d "{ \"name\": \"test action\", \"description\": \"first action adding\", \"action\": [ { \"type\": \"outlet\", \"action\": [ { \"name\": \"PriseVentilateur\", \"field\": \"isOn\", \"fieldType\": \"bool\", \"value\": \"true\"  } ] }, { \"type\": \"light\", \"action\": [ { \"name\": \"LumierePorte\", \"field\": \"color\", \"value\": \"r=255,g=0,b=0\", \"fieldType\": \"string\" } ] } ] }"
```

- PUT /api/v1/orchestrator/actions
- GET /api/v1/orchestrator/actions/list
- DELETE /api/v1/orchestrator/actions

- GET /api/v1/orchestrator/schedules?uuid=
- POST /api/v1/orchestrator/schedules

Example, a schedule to link with a OR rules 12345/23456 and action 12345:

```bash
curl -k -v https://127.0.0.1:16565/api/v1/orchestrator/schedules -X POST -d "{ \"name\": \"Test Schedule\", \"description\": \"This is a test\", \"ruleId\": [ \"12345\", \"23456\" ], \"actionId\": [ \"12345\" ], \"mode\": \"permissive\", \"isActive\": true, \"periodicity\": 0, \"periodicityUnit\": \"seconds\", \"validDay\": [ \"monday\", \"tuesday\", \"wednesday\", \"thursday\", \"friday\" ] }"
```

- GET /api/v1/orchestrator/schedules/list
- DELETE /api/v1/orchestrator/schedules

Field types
-----------

For each type of device (outlet/light), we have different field names:

- "light":

  Condition:
  
|Field|Type|Interval|Description|
|-----|----|--------|-----------|
|`hue`|int|[1;359]|`the hue parameter of the color`|
|`saturation`|int|[0;255]|`the saturation parameter of the color`|
|`isOn`|bool|true/false|`the status on/off of the light`|
|`brightness`|int|[1;255]|`the brightness of the light`|
|`isReachable`|bool|true/false|`if the light is reachable or not`|

  Action:
  
|Field|Type|Possible values|Description|
|-----|----|---------------|-----------|
|`color`|string|`r=x,g=y,b=z` or `hue=x&saturation=y` where x/y/z are integers, or a color name|`To change the light color`|
|`isOn`|bool|`true` or `false`|`To change the light to on or off status`|
|`brightness`|int|`[1;255]`|`To change the light brightness`|
|`effect`|string|`blink`|`To make the light do something, like a blink`|

- "outlet":

  Condition:
  
|Field|Type|Interval|Description|
|-----|----|--------|-----------|
|`isOn`|bool|true/false|`if the outlet is on or off`|
|`power`|float|[0;+inf]|`the outlet power consumption`|
|`isReachable`|bool|true/false|`if the outlet is reachable or not`|
  
  Action:
  
|Field|Type|Possible values|Description|
|-----|----|---------------|-----------|
|`isOn`|bool|`true`/`false`|`To set the outlet on or off`|

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/service/orchestrator.git
