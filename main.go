package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"os"
	"time"
)

const (
	DefaultConfigurationFilePath = "conf.json"
)

func main() {
	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len(os.Args) > 1 {
		configurationFilePath = os.Args[1]
	}

	if configuration, err := service.BuildConfiguration(configurationFilePath); err == nil {
		if _, err := service.BuildService(common.GhostService[common.ServiceOrchestrator].DefaultPort,
			configuration); err == nil {
			for {
				time.Sleep(time.Second)
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println(err)
		os.Exit(1)
	}
}
