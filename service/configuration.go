package service

import (
	"encoding/json"
	"io/ioutil"
)

// configuration
type Configuration struct {
	SecurityManager struct {
		Hostname string `json:"hostname"`

		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securityManager"`

	Directory struct {
		Hostname string `json:"hostname"`
	} `json:"directory"`

	RabbitMQ struct {
		Hostname []string `json:"hostname"`
		Username string   `json:"username"`
		Password string   `json:"password"`
	} `json:"rabbitmq"`

	MongoDB struct {
		Authentication struct {
			Database string `json:"database"`
			UserName string `json:"username"`
			Password string `json:"password"`
		} `json:"auth"`

		Hostname   []string `json:"hostname"`
		Database   string   `json:"database"`
		Collection struct {
			Sensor string `json:"sensor"`

			Rule     string `json:"rule"`
			Action   string `json:"action"`
			Schedule string `json:"schedule"`
			Trigger  string `json:"trigger"`
		} `json:"collection"`
	} `json:"mongodb"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// configuration
	configuration := &Configuration{}

	// load file
	if fileContent, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// unmarshal
		if err := json.Unmarshal(fileContent,
			configuration); err == nil {
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
