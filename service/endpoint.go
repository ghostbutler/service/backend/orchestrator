package service

import (
	"gitlab.com/ghostbutler/tool/service"
)

const (
	APIServiceV1OrchestratorAddRule = common.APIServiceType(common.APIServiceBuiltInLast + iota)
	APIServiceV1OrchestratorRemoveRule
	APIServiceV1OrchestratorEditRule
	APIServiceV1OrchestratorListRule
	APIServiceV1OrchestratorGetRule

	APIServiceV1OrchestratorAddAction
	APIServiceV1OrchestratorRemoveAction
	APIServiceV1OrchestratorEditAction
	APIServiceV1OrchestratorListAction
	APIServiceV1OrchestratorGetAction

	APIServiceV1OrchestratorAddSchedule
	APIServiceV1OrchestratorRemoveSchedule
	APIServiceV1OrchestratorListSchedule
	APIServiceV1OrchestratorGetSchedule
)

var OrchestratorAPIEndpoint = map[common.APIServiceType]*common.APIEndpoint{
	// rules endpoints
	APIServiceV1OrchestratorAddRule: {
		Path:                    []string{"api", "v1", "orchestrator", "rules"},
		Method:                  "POST",
		Description:             "Create a new rule",
		Callback:                endpointNewRule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorRemoveRule: {
		Path:                    []string{"api", "v1", "orchestrator", "rules"},
		Method:                  "DELETE",
		Description:             "Remove a rule based on its UUID",
		Callback:                endpointDeleteRule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorEditRule: {
		Path:                    []string{"api", "v1", "orchestrator", "rules"},
		Method:                  "PUT",
		Description:             "Edit a rule (uuid must be specified)",
		Callback:                endpointEditRule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorListRule: {
		Path:                    []string{"api", "v1", "orchestrator", "rules", "list"},
		Method:                  "GET",
		Description:             "Give a list of rules id",
		Callback:                endpointListRule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorGetRule: {
		Path:                    []string{"api", "v1", "orchestrator", "rules"},
		Method:                  "GET",
		Description:             "Get a full description of a rule (GET uuid)",
		Callback:                endpointDescribeRule,
		IsMustProvideOneTimeKey: false,
	},

	// actions endpoints
	APIServiceV1OrchestratorAddAction: {
		Path:                    []string{"api", "v1", "orchestrator", "actions"},
		Method:                  "POST",
		Description:             "Add new action",
		Callback:                endpointNewAction,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorRemoveAction: {
		Path:                    []string{"api", "v1", "orchestrator", "actions"},
		Method:                  "DELETE",
		Description:             "Delete an action",
		Callback:                endpointDeleteAction,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorEditAction: {
		Path:                    []string{"api", "v1", "orchestrator", "actions"},
		Method:                  "PUT",
		Description:             "Edit an action given its uuid",
		Callback:                endpointEditAction,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorListAction: {
		Path:                    []string{"api", "v1", "orchestrator", "actions", "list"},
		Method:                  "GET",
		Description:             "Lists all actions ids",
		Callback:                endpointListAction,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorGetAction: {
		Path:                    []string{"api", "v1", "orchestrator", "actions"},
		Method:                  "GET",
		Description:             "Get an action based on its id (GET uuid)",
		Callback:                endpointDescribeAction,
		IsMustProvideOneTimeKey: false,
	},

	// schedules endpoints
	APIServiceV1OrchestratorAddSchedule: {
		Path:                    []string{"api", "v1", "orchestrator", "schedules"},
		Method:                  "POST",
		Description:             "Create new schedule",
		Callback:                endpointNewSchedule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorRemoveSchedule: {
		Path:                    []string{"api", "v1", "orchestrator", "schedules"},
		Method:                  "DELETE",
		Description:             "Delete a schedule based on its ID",
		Callback:                endpointDeleteSchedule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorListSchedule: {
		Path:                    []string{"api", "v1", "orchestrator", "schedules", "list"},
		Method:                  "GET",
		Description:             "List all schedules ID",
		Callback:                endpointListSchedule,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1OrchestratorGetSchedule: {
		Path:                    []string{"api", "v1", "orchestrator", "schedules"},
		Method:                  "GET",
		Description:             "Describe schedule based on its id",
		Callback:                endpointDescribeSchedule,
		IsMustProvideOneTimeKey: false,
	},
}
