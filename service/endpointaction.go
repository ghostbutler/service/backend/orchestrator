package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
)

func endpointNewAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// extract content
	content, _ := ioutil.ReadAll(request.Body)

	// action
	var action orchestration.Action

	// try to parse content
	if err := json.Unmarshal(content,
		&action); err == nil {
		// add action
		if err := orchestrator.Mongo.StoreAction(&action,
			true); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"" +
				err.Error() +
				"\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointDeleteAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// try to parse form
	if err := request.ParseForm(); err == nil {
		// extract uuid
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			// try to remove action
			if err := orchestrator.Mongo.DeleteAction(uuid); err != nil {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointEditAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// extract content
	content, _ := ioutil.ReadAll(request.Body)

	// action
	var action orchestration.Action

	// try to parse content
	if err := json.Unmarshal(content,
		&action); err == nil {
		// add action
		if err := orchestrator.Mongo.StoreAction(&action,
			false); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"" +
				err.Error() +
				"\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointListAction(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// get all
	if actionList, err := orchestrator.Mongo.ListAction(); err == nil {
		// marshal
		content, _ := json.Marshal(actionList)

		// write content type
		rw.Header().Add("Content-Type",
			"application/json")

		// send
		_, _ = rw.Write(content)
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
		return http.StatusInternalServerError
	}
	return http.StatusOK
}

func endpointDescribeAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// try to parse request form
	if err := request.ParseForm(); err == nil {
		// extract action uuid
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			// try to get action
			if action, err := orchestrator.Mongo.GetAction(uuid); err == nil {
				// marshal action
				if content, err := json.Marshal(action); err == nil {
					rw.Header().Add("Content-Type",
						"application/json")
					_, _ = rw.Write(content)
				} else {
					rw.WriteHeader(http.StatusInternalServerError)
					return http.StatusInternalServerError
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}
