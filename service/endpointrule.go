package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
)

func endpointNewRule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// extract content
	content, _ := ioutil.ReadAll(request.Body)

	// rule
	var rule orchestration.Rule

	// try to parse content
	if err := json.Unmarshal(content,
		&rule); err == nil {
		// add rule
		if err := orchestrator.Mongo.StoreRule(&rule,
			true); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"" +
				err.Error() +
				"\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointDeleteRule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// try to parse form
	if err := request.ParseForm(); err == nil {
		// extract uuid
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			// try to remove rule
			if err := orchestrator.Mongo.DeleteRule(uuid); err != nil {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointEditRule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// extract content
	content, _ := ioutil.ReadAll(request.Body)

	// rule
	var rule orchestration.Rule

	// try to parse content
	if err := json.Unmarshal(content,
		&rule); err == nil {
		// add rule
		if err := orchestrator.Mongo.StoreRule(&rule,
			false); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":" +
				err.Error() +
				"\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointListRule(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// get all
	if ruleList, err := orchestrator.Mongo.ListRule(); err == nil {
		// marshal
		content, _ := json.Marshal(ruleList)

		// write content type
		rw.Header().Add("Content-Type",
			"application/json")

		// send
		_, _ = rw.Write(content)
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
		return http.StatusInternalServerError
	}
	return http.StatusOK
}

func endpointDescribeRule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// try to parse request form
	if err := request.ParseForm(); err == nil {
		// extract rule uuid
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			// try to get rule
			if rule, err := orchestrator.Mongo.GetRule(uuid); err == nil {
				// marshal rule
				if content, err := json.Marshal(rule); err == nil {
					rw.Header().Add("Content-Type",
						"application/json")
					_, _ = rw.Write(content)
				} else {
					rw.WriteHeader(http.StatusInternalServerError)
					return http.StatusInternalServerError
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}
