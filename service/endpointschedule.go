package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
)

func endpointNewSchedule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// extract content
	content, _ := ioutil.ReadAll(request.Body)

	// schedule
	var schedule orchestration.Schedule

	// try to parse content
	if err := json.Unmarshal(content,
		&schedule); err == nil {
		if err := orchestrator.Mongo.StoreSchedule(&schedule,
			true); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":" +
				err.Error() +
				"\"}"))
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointDeleteSchedule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// try to parse form
	if err := request.ParseForm(); err == nil {
		// extract uuid
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			if err := orchestrator.Mongo.DeleteSchedule(uuid); err != nil {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"" +
					err.Error() +
					"\"}"))
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func endpointListSchedule(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// orchestration server
	orchestrator := handle.(*Service)

	// get all
	if actionList, err := orchestrator.Mongo.ListSchedule(); err == nil {
		// marshal
		content, _ := json.Marshal(actionList)

		// write content type
		rw.Header().Add("Content-Type",
			"application/json")

		// send
		_, _ = rw.Write(content)
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
		return http.StatusInternalServerError
	}
	return http.StatusOK
}

func endpointDescribeSchedule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	orchestrator := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		if uuid := common.ExtractFormValue(request,
			"uuid"); len(uuid) > 0 {
			if action, err := orchestrator.Mongo.GetSchedule(uuid); err == nil {
				if content, err := json.Marshal(action); err == nil {
					rw.Header().Add("Content-Type",
						"application/json")
					_, _ = rw.Write(content)
				} else {
					rw.WriteHeader(http.StatusInternalServerError)
					return http.StatusInternalServerError
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
				return http.StatusBadRequest
			}
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}
