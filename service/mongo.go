package service

import (
	"github.com/globalsign/mgo"
	"time"
)

type Mongo struct {
	// mgo session
	Session *mgo.Session

	// database
	Database string

	// collections
	SensorCollection   string
	RuleCollection     string
	ActionCollection   string
	ScheduleCollection string
	TriggerCollection  string
}

func BuildMongo(configuration *Configuration) (*Mongo, error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    configuration.MongoDB.Hostname,
		Timeout:  10 * time.Second,
		Database: configuration.MongoDB.Authentication.Database,
		Username: configuration.MongoDB.Authentication.UserName,
		Password: configuration.MongoDB.Authentication.Password,
	}
	if mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo); err != nil {
		return nil, err
	} else {
		mongoSession.SetMode(mgo.Monotonic, true)
		mongoSession.SetSafe(&mgo.Safe{WMode: "majority"})
		return &Mongo{
			Session:            mongoSession,
			Database:           configuration.MongoDB.Database,
			SensorCollection:   configuration.MongoDB.Collection.Sensor,
			ActionCollection:   configuration.MongoDB.Collection.Action,
			RuleCollection:     configuration.MongoDB.Collection.Rule,
			ScheduleCollection: configuration.MongoDB.Collection.Schedule,
			TriggerCollection:  configuration.MongoDB.Collection.Trigger,
		}, nil
	}

}

func (mongo *Mongo) Close() {
	mongo.Session.Close()
}
