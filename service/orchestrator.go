package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"time"
)

const (
	DelayBetweenDataUpdate = time.Second
)

type Orchestrator struct {
	// is running?
	isRunning bool

	// rabbit mq handler
	RabbitMQ *rabbit.Controller

	// mongo db handler
	MongoDB *Mongo

	// data
	LastDataUpdate time.Time
	RuleList       []orchestration.Rule
	ActionList     []orchestration.Action
	ScheduleList   []orchestration.Schedule
}

// build orchestrator
func BuildOrchestrator(rabbitMQ *rabbit.Controller,
	mongoDB *Mongo) *Orchestrator {
	orchestrator := &Orchestrator{
		isRunning: true,
		MongoDB:   mongoDB,
		RabbitMQ:  rabbitMQ,
	}
	go orchestrator.updateOrchestrator()
	go orchestrator.updateOrchestratorData()
	return orchestrator
}

func (orchestrator *Orchestrator) updateOrchestratorData() {
	for orchestrator.isRunning {
		orchestrator.pullData()
		time.Sleep(time.Millisecond * 500)
	}
}

func (orchestrator *Orchestrator) updateOrchestrator() {
	for orchestrator.isRunning {
		orchestrator.process()
		time.Sleep(time.Millisecond * 500)
	}
}

func (orchestrator *Orchestrator) testRule(rule *orchestration.Rule) (bool, error) {
	// get collection
	sessionCopy := orchestrator.MongoDB.Session.Copy()
	defer sessionCopy.Close()
	collection := sessionCopy.DB(orchestrator.MongoDB.Database).C(orchestrator.MongoDB.SensorCollection)

	// build mongo requests based on conditions
	if requestList, err := GenerateMongoQuery(rule); err == nil {
		// iterate through built requests
		for _, request := range requestList {
			// do we have a check period (else we do simply consider the last value)
			if request.Content.CheckPeriod > 0 {
				// query documents
				query := collection.Find(request.Mongo).Sort("-time")

				// count documents
				if count, err := query.Count(); err == nil {
					// at least one document?
					if count > 0 {
						result := make([]device.SensorData,
							0,
							1)
						if err := query.All(&result); err == nil {
							for i := 0; i < len(result); i++ {
								if err := result[i].ParseData(); err != nil {
									return false, err
								}
							}
							for _, condition := range request.Content.Condition {
								switch condition.CheckMode {
								default:
									return false, errors.New("bad check mode")

								case orchestration.CheckModePattern:
									if !IsPatternPresent(result,
										condition.Field,
										condition.Comparison,
										condition.Pattern,
										condition.IsStrictPattern) {
										return false, nil
									}
									break
								}
							}
						} else {
							return false, err
						}
					} else {
						return false, nil
					}
				} else {
					return false, err
				}

			} else {
				var s device.SensorData
				if err = collection.Find(request.Mongo).Sort("-time").One(&s); err == nil {
					if err := s.ParseData(); err == nil {
						for _, condition := range request.Content.Condition {
							switch condition.CheckMode {
							case orchestration.CheckModeForComparison:
								if !CheckField(&s,
									condition.Field,
									condition.Value,
									condition.Comparison) {
									return false, nil
								}
								if condition.ForLessThan > 0 {
									if time.Now().Sub(s.Time) >= time.Second*time.Duration(condition.ForLessThan) {
										return false, nil
									}
								}
								if condition.ForMoreThan > 0 {
									if time.Now().Sub(s.Time) <= time.Second*time.Duration(condition.ForMoreThan) {
										return false, nil
									}
								}
								break

							case orchestration.CheckModeLastValue:
								if !CheckField(&s,
									condition.Field,
									condition.Value,
									condition.Comparison) {
									return false, nil
								}
								break
							}
						}
					} else {
						return false, err
					}
				} else {
					return false, err
				}
			}
		}
	} else {
		return false, err
	}
	return true, nil
}

func (orchestrator *Orchestrator) pullData() {
	// update data if needed
	if time.Now().Sub(orchestrator.LastDataUpdate) >= DelayBetweenDataUpdate {
		// todo correct data (set passed/dependencies not satisfied schedules to inactive)

		// pull data
		orchestrator.RuleList, _ = orchestrator.MongoDB.ListRule()
		orchestrator.ActionList, _ = orchestrator.MongoDB.ListAction()
		orchestrator.ScheduleList, _ = orchestrator.MongoDB.ListSchedule()
		//fmt.Println(time.Now().Sub(t))
		orchestrator.LastDataUpdate = time.Now()
	}
}

func (orchestrator *Orchestrator) getRule(uuid string) (*orchestration.Rule, error) {
	ruleList := orchestrator.RuleList
	for _, rule := range ruleList {
		if rule.UUID == uuid {
			return &rule, nil
		}
	}
	return nil, errors.New("rule " +
		uuid +
		" not found")
}

func (orchestrator *Orchestrator) getAction(uuid string) (*orchestration.Action, error) {
	actionList := orchestrator.ActionList
	for _, action := range actionList {
		if action.UUID == uuid {
			return &action, nil
		}
	}
	return nil, errors.New("action " +
		uuid +
		" not found")
}

func (orchestrator *Orchestrator) checkScheduleRule(schedule *orchestration.Schedule) (bool, error) {
	for _, ruleId := range schedule.RuleId {
		if rule, err := orchestrator.getRule(ruleId); err == nil {
			if result, err := orchestrator.testRule(rule); err == nil {
				switch schedule.RuleMode {
				case orchestration.ScheduleRuleModePermissive:
					if result {
						return true, nil
					}
					break

				default:
					fallthrough
				case orchestration.ScheduleRuleModeStrict:
					if !result {
						return false, nil
					}
					break
				}
			} else {
				return false, err
			}
		} else {
			return false, err
		}
	}
	return schedule.RuleMode == orchestration.ScheduleRuleModeStrict, nil
}

func (orchestrator *Orchestrator) checkScheduleTiming(schedule *orchestration.Schedule) bool {
	if !schedule.Time.IsActive {
		return false
	}
	if !schedule.Time.IsTodayWeekdayValid() {
		return false
	}
	if schedule.Time.StartingDate != "" {
		startingDate, _ := time.Parse("02/01/06",
			schedule.Time.StartingDate)
		if time.Now().Before(startingDate) {
			return false
		}
	}
	if schedule.Time.EndingDate != "" {
		endingDate, _ := time.Parse("02/01/06",
			schedule.Time.StartingDate)
		if time.Now().After(endingDate) {
			return false
		}
	}
	if schedule.Time.StartingTime != "" {
		StartingTime, _ := time.Parse("02/01/06 15:04:05",
			time.Now().Format("02/01/06 ")+
				schedule.Time.StartingTime)
		if time.Now().Before(StartingTime) {
			return false
		}
	}
	if schedule.Time.EndingTime != "" {
		EndingTime, _ := time.Parse("02/01/06 15:04:05",
			time.Now().Format("02/01/06 ")+
				schedule.Time.EndingTime)
		if time.Now().After(EndingTime) {
			return false
		}
	}
	return true
}

func (orchestrator *Orchestrator) runAction(action *orchestration.Action) error {
	if channel := orchestrator.RabbitMQ.Channel; channel != nil {
		actionJson, _ := json.Marshal(action)
		return channel.Publish("",
			rabbit.ActionQueueName,
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        []byte(actionJson),
			})
	} else {
		return errors.New("no rabbit channel")
	}
}

func (orchestrator *Orchestrator) triggerSchedule(schedule *orchestration.Schedule) error {
	if err := orchestrator.MongoDB.RegisterTrigger(&orchestration.Trigger{
		IsLock:     true,
		ScheduleID: schedule.UUID,
	}); err == nil {
		fmt.Println("All rules are valid, schedule \"" + schedule.Name + "\" will now activate, trigger is lock!")
		for _, actionId := range schedule.ActionId {
			if action, err := orchestrator.getAction(actionId); err == nil {
				fmt.Println("Running action \"" + action.Name + "\"")
				if err := orchestrator.runAction(action); err != nil {
					return err
				}
			} else {
				return err
			}
		}
	} else {
		return err
	}
	return nil
}

func (orchestrator *Orchestrator) processSchedule(schedule *orchestration.Schedule) error {
	if orchestrator.checkScheduleTiming(schedule) {
		if result, err := orchestrator.checkScheduleRule(schedule); err == nil {
			if result {
				return orchestrator.triggerSchedule(schedule)
			}
		} else {
			return err
		}
	}
	return nil
}

func (orchestrator *Orchestrator) checkTriggerLock(schedule *orchestration.Schedule,
	trigger *orchestration.Trigger) error {
	switch schedule.TriggerReactivationMode {
	case orchestration.ScheduleTriggerReactivationModeInvalidateRule:
		if result, err := orchestrator.checkScheduleRule(schedule); err == nil {
			if !result {
				fmt.Println("Unlocking trigger \"" + schedule.Name + "\"")
				return orchestrator.MongoDB.UnlockTrigger(schedule.UUID)
			}
		} else {
			return err
		}
		break

	default:
		fallthrough
	case orchestration.ScheduleTriggerReactivationModeTimer:
		if time.Now().After(trigger.CreationTime.Add(schedule.Time.GetPeriodicityDuration())) {
			fmt.Println("Unlocking trigger \"" + schedule.Name + "\"")
			return orchestrator.MongoDB.UnlockTrigger(schedule.UUID)
		}
		break
	}
	return nil
}

func (orchestrator *Orchestrator) process() {
	scheduleList := orchestrator.ScheduleList
	for _, schedule := range scheduleList {
		if trigger, err := orchestrator.MongoDB.CheckTrigger(schedule.UUID); err == nil {
			if trigger.IsLock {
				if err := orchestrator.checkTriggerLock(&schedule,
					trigger); err != nil {
					fmt.Println(err)
				}
			} else {
				if err := orchestrator.processSchedule(&schedule); err != nil {
					fmt.Println(err)
				}
			}
		} else {
			if err == mgo.ErrNotFound {
				if err := orchestrator.processSchedule(&schedule); err != nil {
					fmt.Println(err)
				}
			} else {
				fmt.Println(err)
			}
		}
	}
}
