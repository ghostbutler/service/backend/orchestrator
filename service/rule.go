package service

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"time"
)

type MongoQuery struct {
	Mongo   bson.M
	Content *orchestration.RuleContent
}

func GenerateMongoQuery(rule *orchestration.Rule) ([]MongoQuery, error) {
	if err := rule.IsValid(); err == nil {
		output := make([]MongoQuery,
			0,
			1)
		for i := 0; i < len(rule.Content); i++ {
			request := make(bson.M)
			if rule.Content[i].CheckPeriod > 0 {
				request["time"] = bson.M{
					"$gt": time.Now().Add(time.Second * -1 * time.Duration(rule.Content[i].CheckPeriod)),
				}
			}
			request["type"] = rule.Content[i].Type
			if len(rule.Content[i].Name) > 0 {
				if len(rule.Content[i].Name) <= 1 {
					request["data.name"] = bson.RegEx{
						Pattern: rule.Content[i].Name[0],
					}
				} else {
					nameList := make([]interface{},
						0,
						1)
					for _, name := range rule.Content[i].Name {
						nameList = append(nameList,
							bson.M{"data.name": bson.RegEx{
								Pattern: name,
							},
							},
						)
					}
					request["$or"] = nameList
				}
			}
			if rule.Content[i].CheckPeriod > 0 {
				for _, condition := range rule.Content[i].Condition {
					switch condition.CheckMode {
					case orchestration.CheckModeForComparison:
						fallthrough
					case orchestration.CheckModeLastValue:
						break

					case orchestration.CheckModePattern:
						break

					default:
						break
					}
				}
			}
			output = append(output,
				MongoQuery{
					Mongo:   request,
					Content: &rule.Content[i],
				})
		}
		return output, nil
	} else {
		return nil, err
	}
}

// check field
// returns if the field is correct according to test
func CheckField(sensorData *device.SensorData,
	fieldName string,
	value interface{},
	mode orchestration.ComparisonMode) bool {
	switch sensorData.Type {
	case device.CapabilityTypeName[device.CapabilityTypeMusic]:
		switch sensorData.Data.(type) {
		case device.MusicEvent:
			switch fieldName {
			case orchestration.RuleFieldIdentifierMusicSoundName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.MusicEvent).MusicName == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMusicEvent:
				switch value.(type) {
				case string:
					return string(sensorData.Data.(*device.MusicEvent).Event) == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMusicPlayerName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.MusicEvent).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMusicVolume:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.MusicEvent).Volume > int(value.(float64))
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.MusicEvent).Volume < int(value.(float64))
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.MusicEvent).Volume == int(value.(float64))

					default:
						return false
					}

				case int:
					switch mode {
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.MusicEvent).Volume > value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.MusicEvent).Volume < value.(int)
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.MusicEvent).Volume == value.(int)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMusicIsWarning:
				switch value.(type) {
				case bool:
					return sensorData.Data.(*device.MusicEvent).IsWarning == value.(bool)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMusicUrl:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.MusicEvent).URL == value.(string)

				default:
					return false
				}

			default:
				return false
			}
		default:
			return false
		}
	case device.CapabilityTypeName[device.CapabilityTypeGamepad]:
		switch sensorData.Data.(type) {
		case *device.Switch:
			switch fieldName {
			case orchestration.RuleFieldIdentifierGamepadName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.Switch).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierGamepadType:
				switch value.(type) {
				case string:
					return string(sensorData.Data.(*device.Switch).Type) == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierGamepadUUID:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.Switch).UUID == value.(string)

				default:
					return false
				}

			default:
				break
			}

			switch sensorData.Data.(*device.Switch).Type {
			case device.SwitchEventTypeButton:
				switch sensorData.Data.(*device.Switch).Event.(type) {
				case *device.Button:
					switch fieldName {
					case orchestration.RuleFieldIdentifierGamepadButtonName:
						switch value.(type) {
						case string:
							return sensorData.Data.(*device.Switch).Event.(*device.Button).Name == value.(string)

						default:
							return false
						}
					case orchestration.RuleFieldIdentifierGamepadButtonStatus:
						switch value.(type) {
						case string:
							return string(sensorData.Data.(*device.Switch).Event.(*device.Button).Status) == value.(string)

						default:
							return false
						}

					default:
						return false
					}

				default:
					return false
				}
			case device.SwitchEventTypeJoystick:
				switch sensorData.Data.(device.Switch).Event.(type) {
				case *device.Joystick:
					switch fieldName {
					case orchestration.RuleFieldIdentifierGamepadJoystickName:
						switch value.(type) {
						case string:
							return sensorData.Data.(*device.Switch).Event.(*device.Joystick).Name == value.(string)

						default:
							return false
						}
					case orchestration.RuleFieldIdentifierGamepadJoystickX:
						switch value.(type) {
						case float64:
							switch mode {
							case orchestration.ComparisonModeEqual:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).X == value.(float64)
							case orchestration.ComparisonModeLessThan:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).X < value.(float64)
							case orchestration.ComparisonModeGreaterThan:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).X > value.(float64)

							default:
								return false
							}

						default:
							return false
						}

					case orchestration.RuleFieldIdentifierGamepadJoystickY:
						switch value.(type) {
						case float64:
							switch mode {
							case orchestration.ComparisonModeEqual:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).Y == value.(float64)
							case orchestration.ComparisonModeLessThan:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).Y < value.(float64)
							case orchestration.ComparisonModeGreaterThan:
								return sensorData.Data.(*device.Switch).Event.(*device.Joystick).Y > value.(float64)

							default:
								return false
							}

						default:
							return false
						}

					default:
						return false
					}

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}
	case device.CapabilityTypeName[device.CapabilityTypeLight]:
		switch sensorData.Data.(type) {
		case *device.Light:
			switch fieldName {
			case orchestration.RuleFieldIdentifierLightUUID:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.Light).UUID == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.Light).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightType:
				switch value.(type) {
				case string:
					return string(sensorData.Data.(*device.Light).Type) == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightStateHue:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Light).State.HUE == int(value.(float64))
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Light).State.HUE < int(value.(float64))
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Light).State.HUE > int(value.(float64))

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightStateBrightness:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Light).State.Brightness == int(value.(float64))
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Light).State.Brightness < int(value.(float64))
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Light).State.Brightness > int(value.(float64))

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightStateSaturation:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Light).State.Saturation == int(value.(float64))
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Light).State.Saturation < int(value.(float64))
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Light).State.Saturation > int(value.(float64))

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightStateIsOn:
				switch value.(type) {
				case bool:
					return value.(bool) == sensorData.Data.(*device.Light).State.IsOn

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierLightStateIsReachable:
				switch value.(type) {
				case bool:
					return value.(bool) == sensorData.Data.(*device.Light).State.IsReachable

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}
	case device.CapabilityTypeName[device.CapabilityTypeTextToSpeech]:
		switch sensorData.Data.(type) {
		case *device.TTS:
			switch fieldName {
			case orchestration.RuleFieldIdentifierTTSName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.TTS).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierTTSSentence:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.TTS).Sentence == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierTTSVolume:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.TTS).Volume == int(value.(float64))
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.TTS).Volume < int(value.(float64))
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.TTS).Volume > int(value.(float64))

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierTTSLanguage:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.TTS).Language == value.(string)

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}
	case device.CapabilityTypeName[device.CapabilityTypeOutlet]:
		switch sensorData.Data.(type) {
		case *device.Outlet:
			switch fieldName {
			case orchestration.RuleFieldIdentifierOutletName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.Outlet).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletPort:
				switch value.(type) {
				case int:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Port == value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Port < value.(int)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Port > value.(int)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletOutput:
				switch value.(type) {
				case int:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Output == value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Output < value.(int)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Output > value.(int)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletPower:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Power == value.(float64)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Power < value.(float64)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Power > value.(float64)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletEnergy:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Energy == value.(float64)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Energy < value.(float64)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Energy > value.(float64)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletEnabled:
				switch value.(type) {
				case int:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Enabled == value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Enabled < value.(int)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Enabled > value.(int)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletCurrent:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Current == value.(float64)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Current < value.(float64)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Current > value.(float64)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletVoltage:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).Voltage == value.(float64)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).Voltage < value.(float64)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).Voltage > value.(float64)

					default:
						return false
					}

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierOutletPowerFactor:
				switch value.(type) {
				case float64:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.Outlet).PowerFactor == value.(float64)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.Outlet).PowerFactor < value.(float64)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.Outlet).PowerFactor > value.(float64)

					default:
						return false
					}

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}

	case device.CapabilityTypeName[device.CapabilityTypeTemperature]:
		switch sensorData.Data.(type) {
		case *device.TemperatureEvent:
			switch fieldName {
			case orchestration.RuleFieldIdentifierTemperatureName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.TemperatureEvent).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierTemperatureValue:
				switch value.(type) {
				case int:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.TemperatureEvent).Temperature == value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.TemperatureEvent).Temperature < value.(int)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.TemperatureEvent).Temperature > value.(int)

					default:
						return false
					}

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}

	case device.CapabilityTypeName[device.CapabilityTypeMoisture]:
		switch sensorData.Data.(type) {
		case *device.TemperatureEvent:
			switch fieldName {
			case orchestration.RuleFieldIdentifierMoistureName:
				switch value.(type) {
				case string:
					return sensorData.Data.(*device.MoistureEvent).Name == value.(string)

				default:
					return false
				}
			case orchestration.RuleFieldIdentifierMoistureValue:
				switch value.(type) {
				case int:
					switch mode {
					case orchestration.ComparisonModeEqual:
						return sensorData.Data.(*device.MoistureEvent).Moisture == value.(int)
					case orchestration.ComparisonModeLessThan:
						return sensorData.Data.(*device.MoistureEvent).Moisture < value.(int)
					case orchestration.ComparisonModeGreaterThan:
						return sensorData.Data.(*device.MoistureEvent).Moisture > value.(int)

					default:
						return false
					}

				default:
					return false
				}

			default:
				return false
			}

		default:
			return false
		}

	default:
		return false
	}
}

func IsPatternPresent(data []device.SensorData,
	field string,
	mode orchestration.ComparisonMode,
	pattern []orchestration.RulePattern,
	isStrictPattern bool) bool {
	// current value into the given pattern
	patternIndex := 0

	// iterate starting with the older value
	for i := len(data) - 1; i >= 0; i-- {
		if pattern[patternIndex].Name == data[i].Name {
			// is this value matching the pattern?
			if CheckField(&data[i],
				field,
				pattern[patternIndex].Value,
				mode) {
				patternIndex++
			} else {
				// strict pattern means value not found return to 0
				if isStrictPattern {
					patternIndex = 0
				}
			}
		} else {
			if isStrictPattern {
				patternIndex = 0
			}
		}

		// reach the goal?
		if patternIndex >= len(pattern) {
			break
		}
	}

	// did we reach the end of the pattern?
	return patternIndex >= len(pattern)
}
