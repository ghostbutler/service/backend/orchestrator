package service

import (
	"crypto/tls"
	"errors"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"net/http"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// http client
	httpClient *http.Client

	// mongo handler
	Mongo *Mongo

	// rabbit handler
	RabbitMQ *rabbit.Controller

	// is running?
	isRunning bool

	Orchestrator *Orchestrator
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		isRunning:     true,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceOrchestrator].Name),
		httpClient: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		},
	}

	// build mongo handler
	if handler, err := BuildMongo(configuration); err == nil {
		service.Mongo = handler
	} else {
		return nil, errors.New("mongo: " +
			err.Error())
	}

	// build rabbit handler
	if handler, err := rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err == nil {
		service.RabbitMQ = handler
	} else {
		return nil, errors.New("rabbit: " +
			err.Error())
	}

	// build orchestrator
	service.Orchestrator = BuildOrchestrator(service.RabbitMQ,
		service.Mongo)

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceOrchestrator,
		OrchestratorAPIEndpoint,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}
