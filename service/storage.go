package service

import (
	"errors"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"time"
)

func (mongo *Mongo) generateUUID(collectionName string) (string, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(collectionName)
	for {
		uuid := orchestration.GenerateRandomUUID()
		if n, err := collection.Find(bson.D{
			{
				Name:  "basis.uuid",
				Value: uuid,
			},
		}).Count(); err == nil {
			if n <= 0 {
				return uuid, nil
			}
		} else {
			return "", err
		}
	}
}

func (mongo *Mongo) delete(collectionName string,
	uuid string) error {
	session := mongo.Session.Copy()
	collection := session.DB(mongo.Database).C(collectionName)
	return collection.Remove(bson.M{
		"basis.uuid": uuid,
	},
	)
}

func (mongo *Mongo) StoreRule(rule *orchestration.Rule,
	isNew bool) error {
	if err := rule.IsValid(); err != nil {
		return err
	}
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.RuleCollection)
	if isNew {
		if uuid, err := mongo.generateUUID(mongo.RuleCollection); err == nil {
			rule.UUID = uuid
			return collection.Insert(rule)
		} else {
			return err
		}
	} else {
		if oldRule, err := mongo.GetRule(rule.UUID); err == nil {
			return collection.Update(bson.M{
				"basis.uuid": oldRule.UUID,
			},
				rule)
		} else {
			return err
		}
	}
}

func (mongo *Mongo) GetRule(uuid string) (*orchestration.Rule, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.RuleCollection)
	var rule orchestration.Rule
	err := collection.Find(bson.D{
		{
			Name:  "basis.uuid",
			Value: uuid,
		},
	}).One(&rule)
	return &rule, err
}

func (mongo *Mongo) ListRule() ([]orchestration.Rule, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.RuleCollection)
	result := make([]orchestration.Rule, 0, 1)
	err := collection.Find(nil).All(&result)
	return result, err
}

func (mongo *Mongo) DeleteRule(uuid string) error {
	return mongo.delete(mongo.RuleCollection,
		uuid)
}

func (mongo *Mongo) StoreAction(action *orchestration.Action,
	isNew bool) error {
	if err := action.IsValid(); err != nil {
		return err
	}
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ActionCollection)
	if isNew {
		if uuid, err := mongo.generateUUID(mongo.ActionCollection); err == nil {
			action.UUID = uuid
			return collection.Insert(action)
		} else {
			return err
		}
	} else {
		if oldAction, err := mongo.GetAction(action.UUID); err == nil {
			return collection.UpdateId(bson.M{
				"basis.uuid": oldAction.UUID,
			},
				action)
		} else {
			return err
		}
	}
}

func (mongo *Mongo) GetAction(uuid string) (*orchestration.Action, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ActionCollection)
	var action orchestration.Action
	err := collection.Find(bson.D{
		{
			Name:  "basis.uuid",
			Value: uuid,
		},
	}).One(&action)
	return &action, err
}

func (mongo *Mongo) ListAction() ([]orchestration.Action, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ActionCollection)
	result := make([]orchestration.Action, 0, 1)
	err := collection.Find(nil).All(&result)
	return result, err
}

func (mongo *Mongo) DeleteAction(uuid string) error {
	return mongo.delete(mongo.ActionCollection,
		uuid)
}

func (mongo *Mongo) StoreSchedule(schedule *orchestration.Schedule,
	isNew bool) error {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ScheduleCollection)
	if ruleList, err := mongo.ListRule(); err == nil {
		if actionList, err := mongo.ListAction(); err == nil {
			if err := schedule.IsValid(ruleList,
				actionList); err != nil {
				return err
			}
		} else {
			return err
		}
	} else {
		return err
	}
	if isNew {
		if uuid, err := mongo.generateUUID(mongo.ScheduleCollection); err == nil {
			schedule.UUID = uuid
			schedule.Time.IsActive = true
			return collection.Insert(schedule)
		} else {
			return err
		}
	} else {
		if oldSchedule, err := mongo.GetSchedule(schedule.UUID); err == nil {
			return collection.UpdateId(bson.M{
				"basis.uuid": oldSchedule.UUID,
			},
				schedule)
		} else {
			return err
		}
	}
}

func (mongo *Mongo) GetSchedule(uuid string) (*orchestration.Schedule, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ScheduleCollection)
	var schedule orchestration.Schedule
	err := collection.Find(bson.D{
		{
			Name:  "basis.uuid",
			Value: uuid,
		},
	}).One(&schedule)
	return &schedule, err
}

func (mongo *Mongo) ListSchedule() ([]orchestration.Schedule, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.ScheduleCollection)
	result := make([]orchestration.Schedule, 0, 1)
	err := collection.Find(nil).All(&result)
	return result, err
}

func (mongo *Mongo) DeleteSchedule(uuid string) error {
	return mongo.delete(mongo.ScheduleCollection,
		uuid)
}

func (mongo *Mongo) RegisterTrigger(trigger *orchestration.Trigger) error {
	if uuid, err := mongo.generateUUID(mongo.RuleCollection); err == nil {
		session := mongo.Session.Copy()
		defer session.Close()
		collection := session.DB(mongo.Database).C(mongo.TriggerCollection)
		trigger.UUID = uuid
		trigger.CreationTime = time.Now()
		return collection.Insert(trigger)
	} else {
		return err
	}
}

func (mongo *Mongo) UnlockTrigger(scheduleID string) error {
	if schedule, err := mongo.CheckTrigger(scheduleID); err == nil {
		if schedule.IsLock {
			session := mongo.Session.Copy()
			defer session.Close()
			collection := session.DB(mongo.Database).C(mongo.TriggerCollection)
			schedule.IsLock = false
			schedule.UnlockTime = time.Now()
			return collection.Update(bson.M{
				"uuid": schedule.UUID,
			},
				schedule)
		} else {
			return errors.New("schedule " +
				scheduleID +
				" wasn't lock")
		}
	} else {
		return err
	}
}

func (mongo *Mongo) CheckTrigger(scheduleID string) (*orchestration.Trigger, error) {
	session := mongo.Session.Copy()
	defer session.Close()
	collection := session.DB(mongo.Database).C(mongo.TriggerCollection)
	var trigger orchestration.Trigger
	err := collection.Find(bson.M{
		"scheduleID": scheduleID,
	},
	).Sort("-creationTime").One(&trigger)
	return &trigger, err
}
